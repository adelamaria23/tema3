const FIRST_NAME = "Adela-Maria";
const LAST_NAME = "Valsan";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
class Employee {

   constructor(name,surname,salary)
   {
       this.name=name;
       this.surname=surname;
       this.salary=salary;
   }

   getDetails()
   {
       return this.name+" "+this.surname +" "+this.salary;
   }
    

    

    
}

class SoftwareEngineer extends Employee {
   
    constructor(name,surname,salary,experience='JUNIOR')
    {
        super(name,surname,salary);
        this.experience=experience;

    }

    applyBonus()
    {
      switch(this.experience)
      {
         case 'JUNIOR':
            return this.salary+0.1*this.salary;
         case 'MIDDLE':
          return this.salary+0.15*this.salary;       
           
         case 'SENIOR':
            return this.salary+0.2*this.salary;  
         default:
           return this.salary+0.1*this.salary;                     
      }
      
    }
}




module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}
